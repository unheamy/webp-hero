
import {Webp} from "../libwebp/dist/webp.js"
import {loadBinaryData} from "./load-binary-data.js"
import {convertDataURIToBinary, isBase64Url} from "./convert-binary-data.js"

const relax = () => new Promise(resolve => requestAnimationFrame(resolve))

export class WebpMachineError extends Error {}

/**
 * Webp Machine
 * - decode and polyfill webp images
 * - can only decode images one-at-a-time (otherwise will throw busy error)
 */
export class WebpMachine {
	private readonly webp: Webp
	private busy = false

	constructor() {
		this.webp = new Webp()
	}

	/**
	 * Decode raw webp data into a png data url
	 */
	async decode(webpData: Uint8Array): Promise<string> {
		if (this.busy) throw new WebpMachineError("cannot decode when already busy")
		this.busy = true

		try {
			await relax()
			const canvas = document.createElement("canvas")
			this.webp.setCanvas(canvas)
			this.webp.webpToSdl(webpData, webpData.length)
			this.busy = false
			return canvas.toDataURL()
		}
		catch (error) {
			this.busy = false
			error.name = WebpMachineError.name
			error.message = `failed to decode webp image: ${error.message}`
			throw error
		}
	}

	/**
	 * Polyfill the webp format on the given <img> element
	 */
	async polyfillImage(src: string): Promise<string> {
		if (/\.webp.*$/i.test(src)) {
			try {
				const webpData = isBase64Url(src)
					? convertDataURIToBinary(src)
					: await loadBinaryData(src)
				return await this.decode(webpData)
			}
			catch (error) {
				error.name = WebpMachineError.name
				error.message = `failed to polyfill image "${src}": ${error.message}`
				throw error
			}
		} else {
			return src;
		}
	}

	/**
	 * Polyfill webp format on the entire web page
	 */
	async polyfillDocument(): Promise<void> {
		for (const image of Array.from(window.document.querySelectorAll("img,image"))) {
			try {
				if (image instanceof HTMLImageElement)
					image.src = await this.polyfillImage(image.src);
				if (image instanceof SVGImageElement)
					image.href.baseVal = await this.polyfillImage(image.href.baseVal);
			}
			catch (error) {
				error.name = WebpMachineError.name
				error.message = `webp image polyfill failed for "${image}": ${error}`
				throw error
			}
		}
	}
}
